package org.nrg.testing

import org.dcm4che3.data.Attributes
import org.dcm4che3.data.Tag
import org.dcm4che3.data.UID
import org.dcm4che3.io.DicomOutputStream

import java.nio.file.Files
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class DicomZip {

    private ZipOutputStream zipOutputStream
    private File zipFile

    DicomZip(File file = null) {
        zipFile = file ?: Files.createTempFile('dicom', '.zip').toFile()
        zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile))
    }

    DicomZip addInstance(Attributes dicomInstance) {
        final ZipEntry zipEntry = new ZipEntry(dicomInstance.getString(Tag.SOPInstanceUID) + '.dcm')
        zipOutputStream.putNextEntry(zipEntry)

        DicomUtils.writeDicomToDicomOutputStream(
                new DicomOutputStream(
                        zipOutputStream,
                        UID.ExplicitVRLittleEndian
                ),
                dicomInstance
        )
        zipOutputStream.closeEntry()
        this
    }

    File close() {
        zipOutputStream.close()
        zipFile
    }

}
