package org.nrg.testing

import org.apache.commons.lang3.time.StopWatch

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.DateTimeFormatterBuilder
import java.time.temporal.ChronoField

import static org.testng.AssertJUnit.fail

class TimeUtils {

    public static final DateTimeFormatter DICOM_DA_FORMAT = DateTimeFormatter.ofPattern('uuuuMMdd')
    public static final DateTimeFormatter DICOM_TM_FORMAT = tmFormatter()
    public static final DateTimeFormatter MM_DD_YYYY = DateTimeFormatter.ofPattern('MM/dd/uuuu')
    public static final DateTimeFormatter AMERICAN_DATE = MM_DD_YYYY
    public static final DateTimeFormatter AMERICAN_DATE_TIME = DateTimeFormatter.ofPattern('MM/dd/uuuu HH:mm:ss')
    public static final DateTimeFormatter UNAMBIGUOUS_DATETIME = DateTimeFormatter.ofPattern('uuuu-MM-dd HH:mm:ss')
    public static final DateTimeFormatter UNAMBIGUOUS_DATE = DateTimeFormatter.ofPattern('uuuu-MM-dd')
    public static final DateTimeFormatter DEFAULT_TIMESTAMP = DateTimeFormatter.ofPattern('uuuuMMdd_HHmmss')

    static String getTimestamp() {
        getTimestamp(DEFAULT_TIMESTAMP)
    }

    static String getTimestamp(String format) {
        getTimestamp(DateTimeFormatter.ofPattern(format))
    }

    static String getTimestamp(DateTimeFormatter formatter) {
        getTimestamp(formatter, LocalDateTime.now())
    }

    static String getTimestamp(DateTimeFormatter formatter, LocalDateTime dateTime) {
        dateTime.format(formatter)
    }

    static final LocalDate date(int year, int month, int day) {
        LocalDate.parse("${year}-${month}-${day}")
    }

    private static tmFormatter() {
        new DateTimeFormatterBuilder().appendPattern('HH').
                optionalStart().appendPattern('mm').
                optionalStart().appendPattern('ss').
                appendFraction(ChronoField.MICRO_OF_SECOND, 0, 6, true).toFormatter()
    }

    /**
     * Use VERY sparingly (although there certainly are legitimate uses, like when you know XNAT takes X time to do something)
     * @param millis time to wait, in milliseconds
     */
    static void sleep(long millis) {
        Thread.sleep(millis)
    }

    static StopWatch launchStopWatch() {
        final StopWatch stopWatch = new StopWatch()
        stopWatch.start()
        stopWatch
    }

    static void checkStopWatch(StopWatch stopWatch, int timeout, String failureMessage) {
        if (maxTimeReached(stopWatch, timeout)) {
            fail(failureMessage)
        }
    }

    static boolean maxTimeReached(StopWatch stopWatch, int timeout) {
        stopWatch.time / 1000 > timeout
    }

    static String cronEveryNSeconds(int n) {
        "*/${n} * * * * *"
    }

}
