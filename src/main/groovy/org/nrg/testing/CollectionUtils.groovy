package org.nrg.testing

class CollectionUtils {

    static <T> List<T> ofType(Collection<? super T> superList, Class<T> type) {
        superList.findAll { entry ->
            type.isInstance(entry)
        } as List<T>
    }

    /**
     * If collection is not already a list, it is first converted to one. If the list contains duplicate entries a, b, then subset (a, b) will not be formed. Duplicates are detected by delegating to Groovy's .unique(...) method
     * @param collection Collection<T> object to build subsets of size 2
     * @return           List<Tuple2<T, T>> object containing list of subsets
     */
    static <T> List<Tuple2<T, T>> subsetsOfSize2(Collection<T> collection) {
        final List<T> asList = ((collection instanceof List) ? collection : collection.toList()).unique(false)
        final List<Tuple2<T, T>> subsets = []
        (0 ..< asList.size()).each { i ->
            ((i + 1) ..< asList.size()).each { j ->
                subsets << new Tuple2<T, T>(asList[i], asList[j])
            }
        }
        subsets
    }

    static <T, U> Map<U, T> invert(Map<T, U> map) {
        if (map.values().toSet().size() < map.size()) {
            throw new UnsupportedOperationException('To invert map, it must not have any duplicate values.')
        }
        map.collectEntries { key, value ->
            [(value) : key]
        } as Map<U, T>
    }

    static <T, U> List<T> preimage(Map<T, U> map, U targetValue) {
        map.keySet().findAll { key ->
            map.get(key) == targetValue
        }
    }

    static List<String> sortStringsCaseInsensitively(List<String> strings) {
        strings.sort(false, String.CASE_INSENSITIVE_ORDER)
    }

}
