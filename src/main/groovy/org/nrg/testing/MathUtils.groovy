package org.nrg.testing

class MathUtils {

    private static final double EPSILON = 0.00000001

    static boolean doublesEqual(double d1, double d2, double epsilon = EPSILON) {
        Math.abs(d1 - d2) < epsilon
    }

    static int differenceToIntComparison(double difference) {
        if (difference > EPSILON) {
            -1
        } else if (difference < -EPSILON) {
            1
        } else {
            0
        }
    }

    static double percentError(double expected, double actual) {
        100 * Math.abs((actual - expected) / expected)
    }

}
