package org.nrg.testing

import org.dcm4che3.data.Attributes
import org.dcm4che3.io.DicomInputStream
import org.dcm4che3.io.DicomOutputStream

import java.nio.file.Files

class DicomUtils {

    public static final int SMALLEST_DATASET_HEADER = 0x00080000
    public static final String DELIMITER = '\\'
    public static final int[] FMI_ELEMENTS = [
            0x00020001,
            0x00020002,
            0x00020003,
            0x00020010,
            0x00020012,
            0x00020013,
            0x00020016,
            0x00020017,
            0x00020018,
            0x00020026,
            0x00020027,
            0x00020028,
            0x00020031,
            0x00020032,
            0x00020033,
            0x00020035,
            0x00020036,
            0x00020037,
            0x00020038,
            0x00020100,
            0x00020102
    ]

    static int stringHeaderToHexInt(String header) {
        Integer.parseInt(header.replaceAll('[(), ]', ''), 16)
    }

    static String intToSimpleHeaderString(int header) {
        Integer.toHexString(header).toUpperCase().padLeft(8, '0')
    }

    static String intToFullHexString(int header) {
        final String hex = intToSimpleHeaderString(header)
        "(${hex.substring(0, 4)},${hex.substring(4)})"
    }

    static Attributes readDicom(File file) {
        readDicomFrom(file)
    }

    static Attributes readDicom(InputStream inputStream) {
        readDicomFrom(inputStream)
    }

    static File writeDicomToFile(Attributes fullDicom) {
        final File tempDicomFile = Files.createTempFile('dicom', '.dcm').toFile()
        writeDicomToFile(fullDicom, tempDicomFile)
        tempDicomFile
    }

    static void writeDicomToFile(Attributes fullDicom, File file) {
        new DicomOutputStream(file).withCloseable { dicomStream ->
            writeDicomToDicomOutputStream(dicomStream, fullDicom)
        }
    }

    static void writeDicomToDicomOutputStream(DicomOutputStream dicomOutputStream, Attributes fullDicom) {
        final Attributes mainDataset = new Attributes()
        mainDataset.addNotSelected(fullDicom, FMI_ELEMENTS)
        dicomOutputStream.writeDataset(
                new Attributes(fullDicom, FMI_ELEMENTS),
                mainDataset
        )
    }

    static File composeDicomInstanceToZip(List<Attributes> dicomInstances, File zipFile = null) {
        final DicomZip dicomZip = new DicomZip(zipFile)
        dicomInstances.each { dicomInstance ->
            dicomZip.addInstance(dicomInstance)
        }
        dicomZip.close()
    }

    static Attributes clone(Attributes dicom) {
        final Attributes dataset = new Attributes()
        dataset.addAll(dicom)
        dataset
    }

    private static Attributes readDicomFrom(Object input) {
        new DicomInputStream(input).withCloseable { dicomStream ->
            final Attributes extracted = dicomStream.readFileMetaInformation()
            extracted.addAll(dicomStream.readDataset(-1, -1))
            extracted
        }
    }

}
