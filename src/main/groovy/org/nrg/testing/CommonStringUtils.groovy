package org.nrg.testing

import groovy.json.JsonSlurper
import org.testng.AssertJUnit

import static org.apache.commons.lang3.StringUtils.*

class CommonStringUtils {

    static String formatUrl(Object... components) {
        components.collect { component ->
            strip(component.toString(), '/')
        }.join('/')
    }

    static String replaceEach(String base, Map<String, String> replacements) {
        replaceEach(base, replacements.keySet() as String[], replacements.values() as String[])
    }

    static String replaceRecursively(String base, Map<String, String> replacements) {
        String replaced = base
        replacements.each { key, value ->
            replaced = replaced.replace(key, value)
        }
        replaced
    }

    static String stripEnclosingBrackets(String base) {
        stripEnd(stripStart(base, '['), ']')
    }

    static boolean isValidJson(String possibleJson) {
        try {
            new JsonSlurper().parseText(possibleJson) // technically this is a little more permissive than it should be. It's close enough
            true
        } catch (Exception ignored) {
            false
        }
    }

    static void assertJsonValid(String possibleJson) {
        if (!isValidJson(possibleJson)) {
            AssertJUnit.fail('Provided JSON is invalid.')
        }
    }

}
