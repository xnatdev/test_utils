package org.nrg.testing

import org.apache.commons.lang3.StringUtils
import org.testng.IMethodInstance
import org.testng.ITestNGMethod
import org.testng.ITestResult
import org.testng.SkipException

import java.lang.annotation.Annotation

import static org.testng.AssertJUnit.*

class TestNgUtils {

    static String getTestName(ITestNGMethod method) {
        method.methodName
    }

    static Class getTestClass(ITestNGMethod method) {
        method.realClass
    }

    static String getTestClassName(ITestNGMethod method) {
        getTestClass(method).simpleName
    }

    static String getTestName(ITestResult result) {
        getTestName(result.method)
    }

    static Class getTestClass(ITestResult result) {
        getTestClass(result.method)
    }

    static String getTestClassName(ITestResult result) {
        getTestClass(result).simpleName
    }

    static Class getTestClass(IMethodInstance instance) {
        getTestClass(instance.method)
    }

    static String getTestName(IMethodInstance instance) {
        getTestName(instance.method)
    }

    static boolean testFailed(ITestResult result) {
        result.getStatus() == ITestResult.FAILURE
    }

    static boolean checkTestFailingAndEquals(ITestResult result, String test) {
        test == getTestName(result) && testFailed(result)
    }

    static <T extends Annotation> T getAnnotation(ITestNGMethod test, Class<T> annotation) {
        test.getConstructorOrMethod().method.getAnnotation(annotation)
    }

    static void assumeTrue(boolean condition, String message) {
        if (condition) return
        throw new SkipException(message)
    }

    static void assumeFalse(boolean condition, String message) {
        assumeTrue(!condition, message)
    }

    static void assertNonempty(CharSequence string) {
        assertTrue(StringUtils.isNotEmpty(string))
    }

    static void assertBinaryFilesEqual(File expected, File actual) {
        assertEquals('Binary files not equal', FileIOUtils.calculateMD5(expected), FileIOUtils.calculateMD5(actual))
    }

    static void assertEqualsIgnoreSpace(String expected, String actual) {
        assertEquals(expected.replaceAll('\\s+', ''), actual.replaceAll('\\s+', ''))
    }

    static void assertNotEquals(String expected, String actual) {
        assertFalse("Expected strings to not be equal.\nExpected: ${expected}\nActual  : ${actual}", expected == actual)
    }

    static void assertCaseInsensitiveAlphabeticalOrder(List<String> list) {
        assertEquals(CollectionUtils.sortStringsCaseInsensitively(list), list)
    }

}
