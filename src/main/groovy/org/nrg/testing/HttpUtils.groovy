package org.nrg.testing

import io.restassured.RestAssured
import io.restassured.config.HttpClientConfig
import io.restassured.response.Response
import org.apache.commons.io.FileUtils
import org.apache.http.params.CoreConnectionPNames

class HttpUtils {

    static void saveBinaryResponseToFile(Response response, File file) {
        response.then().assertThat().statusCode(200)
        FileUtils.copyInputStreamToFile(response.asInputStream(), file)
    }

    static boolean urlAccessible(String url, int maxWaitInSeconds = 5) {
        try {
            RestAssured.given().config(
                    RestAssured.config().httpClient(
                            HttpClientConfig.httpClientConfig().setParam(CoreConnectionPNames.CONNECTION_TIMEOUT, maxWaitInSeconds * 1000)
                    )
            ).get(url)
            true
        } catch (NoRouteToHostException | ConnectException | SocketTimeoutException ignored) {
            false
        }
    }

}
