package org.nrg.testing

import net.lingala.zip4j.ZipFile
import org.apache.commons.codec.digest.DigestUtils
import org.apache.commons.io.FileUtils
import org.apache.commons.io.filefilter.TrueFileFilter
import org.apache.commons.lang3.StringUtils

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths

class FileIOUtils {

    static String readFile(String filePath) {
        new File(filePath).text
    }

    static String readFile(File file) {
        file.text
    }

    static List<File> listFilesRecursively(File directory) {
        FileUtils.listFiles(directory, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE)
    }

    static void mkdirs(String directoryPath) {
        new File(directoryPath).mkdirs()
    }

    static void mkdirs(Path directoryPath) {
        Files.createDirectories(directoryPath)
    }

    static File writeExceptionToFile(String dirPath, String fileName, Throwable exception) {
        mkdirs(dirPath)
        final File file = Paths.get(dirPath, "${fileName}.txt").toFile()
        final PrintWriter exceptionWriter = new PrintWriter(file)
        exception.printStackTrace(exceptionWriter)
        exceptionWriter.close()
        file
    }

    /**
     * Deletes a directory if it already exists. Simply returns if the directory does not exist
     * @param directoryPath Path to the directory to create
     */
    static void rmdir(String directoryPath) {
        final File dir = new File(directoryPath)
        if (dir.exists()) {
            FileUtils.deleteDirectory(dir)
        }
    }

    static String calculateMD5(File file) {
        DigestUtils.md5Hex(new FileInputStream(file))
    }

    static File loadResource(String fileName) {
        final URI fileUri = getClass().getResource(formatResourceName(fileName)).toURI()
        switch (fileUri.scheme) {
            case 'file' :
                return new File(fileUri)
            case 'jar' :
                final File temp = Files.createTempFile('temp', '').toFile()
                temp.deleteOnExit()
                FileUtils.copyInputStreamToFile(fileUri.toURL().openStream(), temp)
                return temp
            default :
                throw new UnsupportedOperationException("Cannot read resource with scheme ${fileUri.scheme}")
        }
    }

    static InputStream loadResourceAsStream(String fileName) {
        getClass().getResourceAsStream(formatResourceName(fileName))
    }

    static File unzip(File targetParentDirectory, File zip, boolean includeZipNameInPath = false) {
        if (!targetParentDirectory.exists()) {
            mkdirs(targetParentDirectory.toPath())
        }
        if (targetParentDirectory.isDirectory()) {
            final File actualTarget = (includeZipNameInPath) ? targetParentDirectory.toPath().resolve(zip.name[0 ..< -4]).toFile() : targetParentDirectory
            new ZipFile(zip).extractAll(actualTarget.toString())
            actualTarget
        } else {
            throw new UnsupportedOperationException('Second parameter must be a directory to contain the decompressed archive.')
        }
    }

    private static String formatResourceName(String fileName) {
        '/' + StringUtils.stripStart(fileName, '/')
        StringUtils.prependIfMissing(fileName, '/')
    }

}
