package org.nrg.testing

import io.restassured.RestAssured
import groovy.util.logging.Log4j
import org.apache.log4j.Logger

@Log4j
class XnatDownloadServerClient {

    public static final String URL_BASE = 'download.nrg.wustl.edu'
    public static final String SERVER_URL = formUrl()

    void downloadToFile(String remoteFileName, File localFile) {
        log.info("Downloading ${remoteFileName} ...")
        HttpUtils.saveBinaryResponseToFile(RestAssured.given().get("${SERVER_URL}/pub/data/${remoteFileName}"), localFile)
    }

    private static String formUrl() {
        final String asHttps = "https://${URL_BASE}"
        final String asHttpWithPort = "http://${URL_BASE}:8080"

        if (HttpUtils.urlAccessible(asHttps) && RestAssured.given().get(asHttps).statusCode == 200) {
            asHttps
        } else {
            Logger.getLogger(XnatDownloadServerClient).warn("Access to the NRG download site does not appear to be available over the standard URL (${asHttps}). Attempting to use internal URL: ${asHttpWithPort}...")
            if (HttpUtils.urlAccessible(asHttpWithPort)) {
                asHttpWithPort
            } else {
                throw new RuntimeException('Could not reach the NRG download server.')
            }
        }
    }

}
