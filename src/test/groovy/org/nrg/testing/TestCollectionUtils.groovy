package org.nrg.testing

import org.testng.annotations.Test

import static org.nrg.testing.CollectionUtils.*
import static org.testng.AssertJUnit.assertEquals

class TestCollectionUtils {

    @Test
    void testOfType() {
        assertEquals(
                [1, 2, 3],
                ofType([1, 1.5, 2, 2.5, 3, 3.5], Integer)
        )

        assertEquals(
                ['Claudine', 'Molly', 'Louie'],
                ofType(['Claudine', 'Molly', 128, 'Louie'], String)
        )
    }

    @Test
    void testSubsetsOfSize2() {
        assertEquals(
                [],
                subsetsOfSize2(['1'])
        )

        assertEquals(
                [],
                subsetsOfSize2(['1', '1'])
        )

        assertEquals(
                [new Tuple2('1', '2')],
                subsetsOfSize2(['1', '2'])
        )

        assertEquals(
                [new Tuple2('a', 'b')],
                subsetsOfSize2(['a', 'b', 'a', 'b'])
        )

        assertEquals(
                [new Tuple2(1, 2), new Tuple2(1, 3), new Tuple2(2, 3)],
                subsetsOfSize2([1, 2, 3])
        )

        assertEquals(
                [new Tuple2('peach', 'blackberry'), new Tuple2('peach', 'mango'), new Tuple2('peach', 'kiwi'), new Tuple2('blackberry', 'mango'), new Tuple2('blackberry', 'kiwi'), new Tuple2('mango', 'kiwi')],
                subsetsOfSize2(['peach', 'blackberry', 'mango', 'kiwi'])
        )
    }

    @Test
    void testInvert() {
        assertEquals([1 : 'cat', 2 : 'dog'], invert(['cat' : 1, 'dog' : 2]))
        assertEquals(['cat' : 1, 'dog' : 2], invert([1 : 'cat', 2 : 'dog']))
    }

    @Test(expectedExceptions = UnsupportedOperationException)
    void testNoninvertible() {
        invert(['cat' : 1, 'kitten' : 1])
    }

    @Test
    void testSortStringsCaseInsensitively() {
        assertEquals(
                ['cd', 'du -h', 'PWD', 'ssh', 'TREE'],
                sortStringsCaseInsensitively(['ssh', 'PWD', 'cd', 'du -h', 'TREE'])
        )
    }

}
