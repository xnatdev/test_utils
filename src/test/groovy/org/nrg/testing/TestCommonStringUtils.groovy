package org.nrg.testing

import org.testng.annotations.Test

import static CommonStringUtils.*
import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertFalse
import static org.testng.AssertJUnit.assertTrue

class TestCommonStringUtils {

    @Test
    void testFormatUrl() {
        assertEquals(
                'https://xnat.org/projects/XNAT/2016-WORKSHOP',
                formatUrl('https://xnat.org/', '/projects//', '/XNAT', '2016-WORKSHOP/')
        )

        assertEquals(
                'http://wustl.edu/testxnat/data/projects/PROJECT_A/subjects/subject_A',
                formatUrl('http://wustl.edu/testxnat', '/data/projects/PROJECT_A/', '/subjects/subject_A')
        )
    }

    @Test
    void testReplaceEach() {
        assertEquals('CLAUDINE', replaceEach('louie', ['ie' : 'INE', 'lou' : 'CLAUD']))
        assertEquals('23345', replaceEach('12345', ['1' : '2', '2' : '3']))
    }

    @Test
    void testReplaceRecursively() {
        assertEquals('CLAUDINE', replaceRecursively('louie', ['ie' : 'INE', 'lou' : 'CLAUD']))
        assertEquals('33345', replaceRecursively('12345', ['1' : '2', '2' : '3']))
    }

    @Test
    void testStripEnclosingBrackets() {
        assertEquals('Failed', stripEnclosingBrackets('[Failed]'))
        assertEquals('Passed', stripEnclosingBrackets('Passed]'))
        assertEquals('Passed', stripEnclosingBrackets('[[Passed]]'))
        assertEquals('x -> xi[1] + 3', stripEnclosingBrackets('[x -> xi[1] + 3]'))
    }

    @Test
    void testIsValidJson() {
        [
                '{"id": 123123, "keys": ["AZ8-09", "B0-123"]}',
                '[{"id": 4, "dict": {"a": "b", "c": "d"}}]'
        ].each { jsonString ->
            assertTrue(isValidJson(jsonString))
        }
        [
                '{id: 6}',
                '["id": 5]',
                'hello'
        ].each { jsonString ->
            assertFalse(isValidJson(jsonString))
        }
    }

}
