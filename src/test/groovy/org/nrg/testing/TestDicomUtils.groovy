package org.nrg.testing

import org.dcm4che3.data.Tag
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestDicomUtils {

    @Test
    void testStringToInt() {
        assertEquals(Tag.StudyInstanceUID, DicomUtils.stringHeaderToHexInt('(0020,000d)'))
        assertEquals(Tag.SeriesInstanceUID, DicomUtils.stringHeaderToHexInt('(0020,000E)'))
        assertEquals(Tag.StudyDescription, DicomUtils.stringHeaderToHexInt('00081030'))
    }

    @Test
    void testIntToString() {
        assertEquals('0020000D', DicomUtils.intToSimpleHeaderString(Tag.StudyInstanceUID))
        assertEquals('00081030', DicomUtils.intToSimpleHeaderString(Tag.StudyDescription))
    }

    @Test
    void testIntToFullString() {
        assertEquals('(0010,0020)', DicomUtils.intToFullHexString(Tag.PatientID))
        assertEquals('(0008,103E)', DicomUtils.intToFullHexString(Tag.SeriesDescription))
    }

}
