package org.nrg.testing

import org.testng.annotations.Test

import static org.nrg.testing.MathUtils.*
import static org.testng.AssertJUnit.*

class TestMathUtils {

    @Test
    void testDoubleEqualityCheck() {
        assertTrue(doublesEqual(1000, 1000))
        assertFalse(doublesEqual(100, 100.000001))
        assertTrue(doublesEqual(100, 100.0000000001))
    }

    @Test
    void testPercentError() {
        assertTrue(doublesEqual(200, percentError(100, 300)))
        assertTrue(doublesEqual(50, percentError(1000, 500)))
    }

}
