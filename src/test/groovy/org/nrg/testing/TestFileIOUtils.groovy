package org.nrg.testing

import org.testng.annotations.Test

import java.nio.file.Files
import java.nio.file.Path

import static org.testng.AssertJUnit.assertEquals

class TestFileIOUtils {

    @Test
    void testMD5() {
        assertEquals('2c5b6738324c29dbb58d0a7111c8ce32', FileIOUtils.calculateMD5(FileIOUtils.loadResource('testfile.txt')))
    }

    @Test
    void testUnzip() {
        final File simpleZip = FileIOUtils.loadResource('test_zip.zip')
        final File sampleTestDataZip = FileIOUtils.loadResource('test_test_data.zip')
        final Path temp = Files.createTempDirectory('unit_test_temp')
        final Path simpleArchiveDirectory = temp.resolve('test_zip')
        assertEquals(simpleArchiveDirectory.toString(), FileIOUtils.unzip(temp.toFile(), simpleZip, true).toString())
        assertEquals('This is a test file', simpleArchiveDirectory.resolve('testfile.txt').toFile().text.trim())
        assertEquals(temp.toFile(), FileIOUtils.unzip(temp.toFile(), sampleTestDataZip, false))
        assertEquals('Hello I am a DICOM file [not really]', temp.resolve('test_test_data/study_001.dcm').toFile().text.trim())
    }

}
